/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package xogame2;

import java.util.Scanner;

/**
 *
 * @author 66955
 */
public class XOgame2 {

    static char table[][] = {{'-', '-', '-'}, {'-', '-', '-'}, {'-', '-', '-'}};
    static char currentplyer = 'O';
    static int row, col;
    static Scanner kb = new Scanner(System.in);
    static boolean finish = false;
    static int count = 0;

    public static void main(String[] args) {
        showWelcome();
        while (true) {
            showTable();
            showTurn();
            inputRowcol();
            process();
            if (finish) {
                break;
            }
        }

    }

    public static void showWelcome() {

        System.out.println("Welcome to OX Game");
    }

    public static void showTable() {
        for (int i = 0; i < table.length; i++) {
            for (int j = 0; j < table.length; j++) {
                System.out.print(table[i][j] + " ");
            }
            System.out.println("");
        }
    }

    public static void showTurn() {
        System.out.println("Turn " + currentplyer);
    }

    public static void inputRowcol() {
        System.out.println("Please input low,col : ");
        row = kb.nextInt();
        col = kb.nextInt();
    }

    public static void process() {
        if (setTable()) {
            if (checkWin()) {
                finish = true;
                showTable();
                System.out.println(">>> " + currentplyer + " Win <<<");
                return;
            }
            changePlayer();
            if (checkDraw()) {
                finish = true;
                showTable();
                System.out.println(">>> Draw <<<");
                return;
            }
            count++;
        }
    }

    public static void changePlayer() {
        if (currentplyer == 'O') {
            currentplyer = 'X';

        } else {
            currentplyer = 'O';
        }
    }

    public static boolean setTable() {
        table[row - 1][col - 1] = currentplyer;
        return true;
    }

    public static boolean checkWin() {
        if (checkCol()) {
            return true;
        } else if (checkRow()) {
            return true;
        } else if (checkX1()) {
            return true;
        } else if (checkX2()) {
            return true;
        }

        return false;
    }

    public static boolean checkRow() {
        for (int r = 0; r < table.length; r++) {
            if (table[r][col - 1] != currentplyer) {
                return false;
            }
        }
        return true;
    }

    public static boolean checkCol() {
        for (int c = 0; c < table.length; c++) {
            if (table[row - 1][c] != currentplyer) {
                return false;
            }
        }
        return true;
    }

    public static boolean checkX1() { //11, 22 ,33
        for (int i = 0; i < table.length; i++) {
            if (table[i][i] != currentplyer) {
                return false;
            }
        }
        return true;
    }

    public static boolean checkX2() { //13, 22 ,31
        for (int i = 0; i < table.length; i++) {
            if (table[i][2 - i] != currentplyer) {
                return false;
            }
        }
        return true;
    }

    public static boolean checkDraw() {
        if (count == 8) {
            return true;
        }
        return false;
    }

}
